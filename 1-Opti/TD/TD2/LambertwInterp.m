function w=LambertwInterp(z)

persistent z_grid w_grid
if isempty(w_grid)
    disp('chargement donnees pour interpolation')
    load data_lambertw.mat
end

w=interp1(z_grid,w_grid,z,'spline');
