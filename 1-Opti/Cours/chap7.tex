\documentclass[main.tex]{subfiles}
\begin{document}
\section{Sous-différentielle}
\subsection{Présentation}
\begin{defin}
  Soit\(f:\mathcal{H} \to \mathbb{R}|\) une fonction propre. La \emph{sous différentielle} (de Moreau) notée $\partial f$ est définie comme:
  \[  \partial f:
    \begin{cases}
      \mathcal{H} \to 2^{\mathcal{H}} \\
      x \mapsto \{u \in \mathcal{H} | (\forall y\in \mathcal{H}) , \langle y- x | u\rangle + f(x) \le f(y)\}
    \end{cases}
  \]
On dit que  $u \in \partial f(x)$ est un \emph{sous-gradient} de $f(x)$.
\end{defin}
La sous différentiel généralise la dérivée aux fonction propre non différentiable (dérivée non continue), ainsi le sous-différentiel d'une fonction convexe est l'ensemble des pentes de toutes les minorantes affines de la fonction.
\begin{figure}[H]
  \centering
  \begin{tikzpicture}
    \draw[-latex] (-2,0) -- (2,0) node[above]{$\mathcal{H}$};
    \draw[-latex] (0,-0.5) -- (0,2.5) node[above,blue]{$f(y)$};
    \draw[domain=-2:0,smooth,thick, blue] plot ({\x},{0.5*\x*\x});
    \draw[thick,blue] (0,0) -- (2,2);
  \end{tikzpicture}%
  \begin{tikzpicture}
    \draw[-latex] (-2,0) -- (2,0) node[above]{$\mathcal{H}$};
    \draw[-latex] (0,-1) -- (0,2.5) node[above]{$u$};
    \draw[thick,blue] (-1,-1) -- (0,0) -- (0,1) -- (1,1);
  \end{tikzpicture}
  \caption{Exemple de sous différentiel, un ensemble de valeur est possible en 0}\label{fig:sous_diff}
\end{figure}

\begin{prop}
  \begin{itemize}
    \item  Si $0 \in \partial f(x)$, alors $x\in \arg\min f(x)$ (Règle de Fermat)
    \item Si $x\notin \dom f$, alors $\partial f(x) =\emptyset$.
    \item $\forall x\in \dom f, \partial f(x)$ est un ensemble convexe fermé.
    \item La sous-différentielle est un opérateur monotone ie:
      \[\forall x_{1},x_{2} \in \mathcal{H}^{2},\forall u_{1},u_{2}\in \partial f(x_{1})\times \partial f(x_{2}) , \langle u_{1}-u_{2} | x_{1}-x_{2} \rangle \ge 0\]
  \end{itemize}
\end{prop}

\begin{prop}
  Si $f$ est Gâteau-différentiable en $x$, alors $\partial f(x) = \{\nabla f(x)\}$.
\end{prop}
\begin{defin}
  Soit $C$ un ensemble convexe de $\mathcal{H}$, alors $\partial\imath_{C}(x)$ est le \emph{cône normal} de $C$ en $x$ défini comme:
  \[N_{C}(x) =
    \begin{cases}
      \{ u\in \mathcal{H} | \forall y \in C, \langle u| y-x \rangle \le 0\} & \text{si }x \in C \\
      \emptyset & \text{sinon}
    \end{cases}
\]
\end{defin}

\begin{figure}[H]
  \centering
  \begin{tikzpicture}
    \draw[fill=blue!20!white, draw] (0,0) -- (1,2) -- (2,1) -- (1.5,-0.5) -- cycle;
    \shade[right color =red!50, left color=white, shading angle=135] (0,0) -- (-2,1) -- (-0.5,-1.5) -- cycle;
    \draw[-latex] (0,0) -- (45:-1) node[above]{$u$} node[above=1em]{\footnotesize$N_{c}(x)$};
  \end{tikzpicture}%
  \hspace{3em}
  \begin{tikzpicture}
    \draw[fill=blue!20!white, smooth cycle]  plot coordinates {(0,0)  (1,2) (2,1) (1.5,-0.5)};
    \draw[-latex] (1.65,1.62) -- ++(45:1) node[above]{$u$};
    \draw[dashed,color=red, path fading=east, fading angle=45] (1.65,1.62) -- ++(45:2);
    \draw[densely dashed,gray] (1.65,1.62) -- ++(-45:1) (1.65,1.62) -- ++(135:1);
  \end{tikzpicture}
  \caption{Cone normal pour un ensemble convexe et strictement convexe}
\end{figure}

\begin{rem}
  Si $x\in \text{int} C,  N_{C}(x) =\{0\}$  et si $C$ est un espace vectoriel , on a $\forall x, N_{C}(x) = C^{\perp}$.
\end{rem}
\subsection{Calcul de sous-différentiels}
\begin{prop}
  Soit $\mathcal{H},\mathcal{g}$ deux espace de Hilbert. Soit $h:\mathcal{H} \to \mathbb{R}| $, $g:\mathcal{g} \to \mathbb{R}|$ et $L\in \mathcal{B}(\mathcal{H},\mathcal{g})$. Alors:
  \begin{itemize}
    \item $\partial f(\lambda x) = \lambda \partial f(x), \forall \lambda > 0$
    \item Si $\dom f \cap \dom g \neq \emptyset$, alors $\forall x \in \mathcal{H}$:
      \[
      \partial f(x) + L^{*}\partial g(Lx) \subset \partial(f+g\circ L) (x)
      \]
  \end{itemize}
\end{prop}
On en particulier:
\begin{itemize}
  \item $\partial f + \partial g = \partial (f+g)$ si $g$ est borné \\
    \item $L^{*}\partial g L = \partial (g\circ L)$ si $\dom g \cap \text{Im} L \neq \emptyset$
\end{itemize}

\begin{prop}
  Soit $(\mathcal{H}_{i})_{i\in I}$ une famille de sous espace de Hibert tel que: $\mathcal{H} = \bigoplus_{i\in I}\mathcal{H}_{i}$ et $\forall i \in I$ soit $f_{i}:\mathcal{H}\to \mathbb{R}|$ une famille de fonction propre telle que:
  \[
    f : \mathcal{H} \to \mathbb{R} : x = (x_{i})_{i\in I} \mapsto \sum_{i\in I}^{}f_{i}(x_{i})
  \]
  Alors:
  \[
    \partial f(x) = \bigtimes_{i\in I} \partial f_{i}(x_{i})
  \]
\end{prop}
\subsection{Conjugué}
\textbf{Rappel} La conjugué d'une fonction est définie comme: $f^{*}(u)=\sup_{x\in\mathcal{ H}} \langle x|u\rangle -f(x)$.  On a un cas d'égalité de l'inégalité de Fenchel-Young:
\begin{prop}[Fenchel-Young]
  \begin{enumerate}
    \item $f(x)+f^{*}(u) \ge \langle x|u\rangle$
    \item $u\in\partial f(x) \iff f(x)+ f^{*}(u) = \langle x|u\rangle$
  \end{enumerate}
\end{prop}


\begin{prop}
  soit $f \in \Gamma_{0}(\mathcal{H})$ alors:
  \[ u\in \partial f(x) \iff x\in \delta f^{*}(u) \]
\end{prop}

\section{Opérateur proximal}
\subsection{Définition}
\begin{defin}
  Soit $f\in \Gamma_{0}(\mathcal{H})$.
  \begin{itemize}
    \item l' \emph{enveloppe de Moreau} de $f$ de paramètre $\gamma >0 $ est:
      \[
      ^{\gamma}f: \mathcal{H} \to \mathbb{R}: x \mapsto \inf_{y\in \mathcal{H}} f(y) + \frac{1}{2\gamma} \|y-x\|^{2}
      \]
    \item L' \emph{opérateur proximal} de $f$ est
      \[  \prox_{f} \mathcal{H} \to \mathcal{H}: x\mapsto \arg\min_{y\in \mathcal{H}} f(y)+ \frac{1}{2} \|y-x\|^{2}\]
  \end{itemize}
\end{defin}

\begin{figure}[H]
  \centering
  \begin{tikzpicture}
    \draw[-latex] (-2,0) -- (2,0) node[above]{$\mathcal{H}$};
    \draw[-latex] (0,-0.5) -- (0,2.5) node[above,blue]{$f(x)$};
    \draw[domain=-2:2,smooth,thick, blue] plot ({\x},{abs(\x)});
  \end{tikzpicture}
  \begin{tikzpicture}
    \draw[-latex] (-2,0) -- (2,0) node[above]{$\mathcal{H}$};
    \draw[-latex] (0,-0.5) -- (0,2.5) node[above,blue]{$^{\gamma}f(x)$};
    \draw[domain=-2:2,dashed] plot ({\x},{abs(\x)});
    \draw[domain=-2:-0.4, thick, blue] plot({\x},{abs(\x)-0.2});
    \draw[domain=0.4:2, thick, blue] plot({\x},{abs(\x)-0.2});
    \draw[domain=-0.4:0.4,thick,blue] plot({\x},{1.25*\x*\x});
  \end{tikzpicture}
  \begin{tikzpicture}
    \draw[-latex] (-2,0) -- (2,0) node[above]{$\mathcal{H}$};
    \draw[-latex] (0,-1.5) -- (0,1.5) node[above,blue]{$\prox_{f}(x)$};
    \draw[domain=-2:-0.8,thick, blue] plot ({\x},{\x +0.8});
    \draw[domain=0.8:2,thick, blue] plot ({\x},{\x -0.8});
    \draw[domain=-0.8:0.8,thick, blue] plot ({\x},0);
  \end{tikzpicture}
  \caption{Enveloppe de Moreau et opérateur proximal}
  \label{fig:moreau_prox}
\end{figure}
\subsection{Caractérisation}
\begin{prop}[existence]
  Soit $f \in \Gamma_{0}(\mathcal{H})$ et $\gamma > 0$.
  \[
    \forall x\in \mathcal{H} ,\exists! p\in \mathcal{H}, f(p) + \frac{1}{2\gamma} \|p-x\|^{2} = \inf_{y\in \mathcal{H}} f(y) + \frac{1}{2\gamma} \|y-x\|^{2}
  \]
\end{prop}
\begin{preuve}
  utiliser l'inégalité de fenchel young pour minorer $f$ et montrer que $f(y)+(2\gamma)^{-1}\|y-x\|^{2}$ est coercive. cette fonctoin est aussi strictement convexe, elle admet donc un unique minimiseur
\end{preuve}
\begin{prop}
  soit $f \in \Gamma_{0}(\mathcal{H})$.
  \[
    \forall x \in \mathcal{H} , p =\prox_{f}(x) \iff x-p \in \partial f (p)
  \]
\end{prop}
\begin{preuve}
  Utiliser la règle de Fermat sur l'opérateur proximal
\end{preuve}
\subsection{Exemples}
\subsubsection{Projection}
Pour un espace convexe $C$on a:
\[
\prox_{\iota_{C}}(x) = \arg\min\frac{1}{2}\|y-x\|^{2} = P_{C}(x)
\]

\begin{figure}[H]
  \centering
  \begin{tikzpicture}
    \draw[fill=blue!20!white, smooth cycle]  plot coordinates {(0,0)  (1,2) (2,1) (1.5,-0.5)};
    \draw[-latex] (1.65,1.62) -- ++(45:1) node[above]{$u$};
    \draw[dashed,color=red, path fading=east, fading angle=45] (1.65,1.62) -- ++(45:2);
    \draw[densely dashed,gray] (1.65,1.62) -- ++(-45:1) (1.65,1.62) -- ++(135:1);
  \end{tikzpicture}
  \caption{\label{fig:label} Projection sur un espace convexe }
\end{figure}

La projection est la distance minimale  d'un point à un ensemble, on retrouve l'idée de ``proximité''.
On a également:
\[
p = P_{C}(x) \iff x-p \in \partial \iota_{C}(p) =N_{C}(p) \iff p\in C , \forall y\in C \langle y-p |x-p \rangle \le 0
\]
Et si $C$ est un espace vectoriel:

\[
p =P_{C}(x) \iff
\begin{cases}
  p \in C \\
  x-p \in C^{\perp}
\end{cases}
\]

Pour l'indicatrice d'un convexe, l'enveloppe de Moreau est définie comme:
\[
 \gamma_{\iota_{C}} = (2\gamma)^{-1} d_{C}^{2} = (2\gamma)^{-1} \inf \|y-x \| = (2\gamma)^{-1}\|x-P_{C}(x)\|
\]
\subsubsection{Normes}
Soit $\chi >0$ et $q \ge 1$. On considère $\varphi:\mathbb{R} \to \mathbb{R}| : \xi \mapsto \chi |\xi|^{q}$ Alors:

\[
\operatorname{prox}_{\varphi} \xi=\left\{\begin{array}{ll}
\operatorname{sign}(\xi) \max \{|\xi|-\chi, 0\} & \text { if } q=1 \\
\xi+\frac{4 \chi}{3 \cdot 2^{1 / 3}}\left((\epsilon-\xi)^{1 / 3}-(\epsilon+\xi)^{1 / 3}\right) \\
\text { where } \epsilon=\sqrt{\xi^{2}+256 \chi^{3} / 729} & \text { if } q=\frac{4}{3} \\
\xi+\frac{9 \chi^{2} \operatorname{sign}(\xi)}{8}\left(1-\sqrt{1+\frac{16|\xi|}{9 \chi^{2}}}\right) & \text { if } q=\frac{3}{2} \\
\frac{\xi}{1+2 \chi} & \text { if } q=2 \\
\operatorname{sign}(\xi) \frac{\sqrt{1+12 \chi|\xi|}-1}{6 \chi} & \text { if } q=3 \\
\left(\frac{\epsilon+\xi}{8 \chi}\right)^{1 / 3}-\left(\frac{\epsilon-\xi}{8 \chi}\right)^{1 / 3} \quad \text { where } \quad \epsilon=\sqrt{\xi^{2}+1 /(27 \chi)} & \text { if } q=4
\end{array}\right.
\]

\subsubsection{Fonction Quadratique}

\begin{prop}
  Soit $\mathcal{H} , \mathcal{G}$ deux espace de hilbert et $L \in \mathcal{B}(\mathcal{H},\mathcal{G})$, $\gamma > 0$ et $z\in\mathcal{G}$.
  \[
    f(x) = \gamma \|Lx -z \|^{2}/2 \implies \prox_{f}(x) (Id+\gamma L^{*}L)^{-1}(x+\gamma L^{*}z)
  \]
\end{prop}
\begin{preuve}
  On a $p=\prox_{f}(x) \iff x-p \in \partial f (p)$. De plus $f$ est Gateau différentiable, donc: $\nabla f(p) = \gamma L^{*}(Lp-z)$
  \[
    x - p  = \gamma L^{*}(Lp-z) \iff p = (Id +\gamma L^{*}L)^{-1}(x+\gamma L^{*}z)
  \]
\end{preuve}

\subsection{Propriété}
\subsubsection{Calcul de proximaux}
On a les règles de calculs suivantes:
\begin{center}
  \begin{tabular}{lcc}
    Properties& $f(x)$ &$\prox_{f}(x)$ \\ \hline \hline
    Translation & $f(x-z),z \in \mathcal{H}$& $z+ \prox_{f}(x-z)$\\ \hline
    Quadratic Perturbation & $\begin{array}{c}
f(x)+\alpha\|\times\|^{2} / 2+\langle z \mid x\rangle+\gamma \\
z \in \mathcal{H}, \alpha>0, \gamma \in \mathbb{R}
\end{array}$
 & $\prox_{\frac{f}{\alpha+1}}\left(\frac{x-z}{\alpha+1}\right)$ \\ \hline
    Scaling & $f(\rho x) , \rho > 0$ & $\frac{1}{\rho} \prox_{\rho^{2}f}(\rho x)$\\ \hline
    Reflection & $f(-x)$ & $-\prox_{f}(-x)$\\ \hline
    Moreau enveloppe & $\begin{array}{c}
\gamma f(x)=\inf _{y \in \mathcal{H}} f(y)+\frac{1}{2 \gamma}\|x-y\|^{2} \\
\gamma>0
\end{array}$ & $\frac{1}{1+\gamma} \gamma x + \prox_{(1+\gamma)f}(x)$ \\ \hline
  \end{tabular}
\end{center}




Par ailleurs, on peux également montrer les résultats suivants:
\begin{prop}
  Soit $(\mathcal{H}_{i})_{1,\ldots,n}$ une famille d'espace de Hilbert et $(f_{i}\in\Gamma_{0}(\mathcal{H}_{i}))$ , si
  \[\forall x = (x_{1},\ldots, x_{n}) \in \bigtimes_{i=1}^{n} \mathcal{H}_{i}\quad f(x) = \sum_{i=1}^{n}f_{i}(x_{i})
  \]
  Alors
  \[
    \forall x = (x_{1},\ldots, x_{n} \prox_{f}(x) = (\prox_{f_{i}}(x_{i}))_{1,\ldots, n}
  \]
\end{prop}
si l'on considère un espace séparable on même:
\begin{prop}
  Soit $(b_{i})$ une base orthonormale de $\mathcal{H}$. On pose
  \[ f(x) = \sum_{i\in I} \phi_{i}(\langle x|b_{i}\rangle) \quad \text{ avec } \phi_{i}\in \Gamma_{0}(\mathbf{R}) \text{ et } \phi_{i} \ge 0
  \]
  Alors:
  \[
    \prox_{f}(x) = \sum_{i\in I}^{}\prox_{\phi_{i}}(\langle x|b_{i}\rangle)b_{i}
  \]
\end{prop}
Et si l'on se place dans un espace de dimension finie, l'hypothèse $\phi_{i}\ge 0$ n'est plus nécessaire.
\begin{exemple}
  pour $\mathcal{H} = \mathbb{R}^{n}$ et $f(x)= \lambda \| x \|_{1}$ avec $\lambda \ge 0$. Alors:
  \[
    \prox_{\lambda\|\cdot\|_{1}} = (\prox_{\lambda |\cdot| }(x^{(i)}))_{1\le i\le n}
  \]
\end{exemple}
\begin{thm}[décomposition de Moreau]
  Soit $f \in \Gamma_{0}(\mathcal{H})$ et $\gamma >0$ Alors:
  \[
    (\forall x \in \mathcal{H}) \quad \prox_{\gamma f^{*}}(x) = x - \gamma \prox_{\gamma^{-1}f}(\gamma^{-1}x)
  \]
\end{thm}

\begin{prop}
  Soit $f \in \Gamma_{0}(\mathcal{H})$ et $L \in \mathcal{B}(\mathcal{H},\mathcal{H})$ un opérateur unitaire\footnotemark. Alors:
  \[
    \prox_{f\circ L} = L^{*} \circ \prox_{f} \circ L
  \]
\end{prop}
\footnotetext{$U^{*}U=UU^{*}=I$}

\subsubsection{support et proximaux}
\begin{prop}
  Soit $C \subset \mathcal{H}$ un ensemble fermé convexe non vide.
  \[
\forall x \in \mathcal{H} \prox_{\sigma_{C}} = Id - P_{C}  \]
\end{prop}
\begin{exemple}
  En dimension 1: $\mathcal{H} = \mathbb{R}$ et $C = [\delta_{1},\delta_{2}]$
  \[
\sigma_{C}(x)=\left\{\begin{array}{ll}
\delta_{1} x & \text { if } x<0 \\
0 & \text { if } x=0 \\
\delta_{2} x & \text { if } x>0
\end{array} \Rightarrow \prox_{\sigma_{C}}(x)=\operatorname{soft}_{C}(x)=\left\{\begin{array}{ll}
x-\delta_{1} & \text { if } x<\delta_{1} \\
0 & \text { if } x \in C \\
x-\delta_{2} & \text { if } x>\delta_{2}
\end{array}\right.\right.
  \]
  \begin{figure}[H]
    \centering
    \begin{tikzpicture}
    \draw[-latex] (-2,0) -- (2,0) node[above]{$\mathcal{H}$};
    \draw[-latex] (0,-1.5) -- (0,1.5) node[above,blue]{$P_{C}(x)$};
    \draw[dashed] (-1,-1.5) -- ++(0,3);
    \draw[dashed] (1.5,-1.5) -- ++(0,3);
    \draw[thick, blue] (-2,-1) -- (-1,-1) -- (1.5,1.5) -- (2,1.5);
  \end{tikzpicture}%
  \hspace{3em}
    \begin{tikzpicture}
    \draw[-latex] (-2,0) -- (2,0) node[above]{$\mathcal{H}$};
    \draw[-latex] (0,-1.5) -- (0,1.5) node[above,blue]{$\prox(x)$};
    \draw[dashed] (-1,-1.5) -- ++(0,3);
    \draw[dashed] (1.5,-1.5) -- ++(0,3);
    \draw[blue, thick] (-2,-1) -- (-1,0) -- (1.5,0) -- (2,1);
    \end{tikzpicture}
  \end{figure}
\end{exemple}

\begin{rem}
  Dans un contexte bayesien, la solution du maximum \emph{a posteriori} est $\prox_{f}(x)$ !
\end{rem}

\section{Exercices}
\subsection*{Exercice 1}
\subsection*{Exercice 2}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "francais"
%%% End:
