Dans ce dépot, l'ensemble de mes cours pour le M2 EEA, et même un peu plus. 

Générer les pdf :
 1) Pour récuper les pdf s'assurer d'avoir `latexmk` d'installé et tout les packages utiles(dans le doute `sudo apt-intall texlive-full latexmk python3-pygments`)
 2) exécuter `./compile.sh` à la racine du dépot
 3) take a coffee

Voir la dernière mise à jour: [ici](https://perso.crans.org/comby/m2-atsi)

Ne pas oublier de visiter la [page wiki du département](https://wiki.crans.org/VieEns/LesDépartements/DépartementEea)
