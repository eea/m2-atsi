function bigI = imGrid(I,xlabels, ylabels)
% plot a series of img side by side, and adds legend. 
%  

[N,M] = size(I);
[n,m] = size(I{1});

bigI = NaN([N*n,M*m],'single');

for ii= 1:N
    for jj=1:M
        bigI((ii-1)*n+1:ii*n,(jj-1)*m+1:jj*m)=single(I{ii,jj});
    end
end
figure;
imagesc(bigI);
axis image;
xticks((0:N)*n +n/2);
xticklabels(xlabels);
yticks((0:M-1)*m +m/2);
yticklabels(ylabels);
end

