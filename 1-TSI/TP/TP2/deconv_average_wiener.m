%%% Deconvolution par le filtre de Wiener. 
%%% (On suppose que le bruit est blanc)
%%% Attention ! Les DSP signal et bruit sont estimées à partir de l'image 
%%% nette et du filtre. 
%%%
%%% usage : y=deconv_wiener(im,OTF,im_nette,SNR) 
%%% 
%%% Entree:
%%% im : image d'entree
%%% OTF : OTF (réponse en fréquence) du filtre
%%% im_nette : image non déconvoluée
%%% SNR : niveau de bruit, supposé blanc, en dB par rapport à l'image nette
%%% convoluée par OTF
%%% Sortie:
%%% y : image déconvoluée
%%% 

function y=deconv_average_wiener(im,OTF,OTFall,im_nette,SNR)

%%% Dimensions de l'image (on suppose qu'elle est carrée)
[M N]=size(im_nette);

%%% DSP image nette, moyenne nulle
Sxx = abs(fft2(im_nette)).^2;
Sxx=Sxx/sum(sum(Sxx));
Sxx(1,1)=0;
tf_y = zeros(size(im));
%%% DSP bruit
E_f = sum(sum(abs(OTF).^2.*Sxx));
s2 = E_f * 10^(-SNR/10);
Sbb = s2 / N^2;
%%% TF de l'image
tf_im = fft2(im);
%%% Calcul de l'image déconvoluée
tf_y = tf_im .*  sum(conj(OTFall),3)/3 .* Sxx ./ (sum(abs(OTFall).^2,3)/3 .* Sxx + Sbb);


tf_y(1,1) = tf_im(1,1); % traitement particulier pour la moyenne

y=real(ifft2(tf_y));



