function PSF = Calcule_PSF( pupille_complexe)
%-------------------------------------------------------------------------%
% USAGE : PSF = Calcule_PSF(pupille_complexe)
% DESCRIPTION :
%  calcul de la PSF a partir de la fonction complexe de la pupille du
%  systeme optique
% ENTREE :
%  pupille_complexe =  matrice de la fonction pupille
% SORTIE :
%  PSF =  image de la PSF
%-------------------------------------------------------------------------%

%La PSF est le resultat de la TF2D de l'autocorrelation de la pupille i.e.
%le module carre de la TF2D de la pupille :
PSF_brute = abs(fft2(ifftshift(ifftshift(pupille_complexe,1),2))).^2;

%PSF reelle (normalis�e par rapport � son �nergie) :
PSF = PSF_brute / sum(sum(PSF_brute));
end

