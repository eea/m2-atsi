%%% Calcul de la qualit� de reconstruction (RQ)
%%%
%%% usage : y=RQ(u,x) 
%%% 
%%% Entree:
%%% u: image d'entree
%%% x: image de reference
%%% NB: u et x doivent avoir la m�me dimension
%%% Sortie:
%%% y : valeur de la RQ
%%% 

function y=RQ(u,x)

% Variance de x
VAR_x = var(x(:));

% Ecart quadratique
Eout = var( x(:) - u(:) );

% Calcul de RQ

y = 10 * log(VAR_x/Eout) / log(10);



