function pupille_complexe = Fonction_Pupille(param_masque,psi,Nb)
%--------------------------------------------------------------------------
% USAGE : 
%  pupille_complexe = Fonction_Pupille(param_masque,psi_plage,Nb)

% DESCRIPTION :
%  Calcul la fonction complexe de la pupille avec un masque de parametre 
%  param_masque et une valeur defocus (psi en lambda PV). On calcule la 
%  pupille sur une grille NbxNb de meme dimension que la scene observee et 
%  que la PSF.
% ENTREES : 
% param_masque = liste des rayons normalises 
% psi = liste  des valeurs du defocus \psi en \lambda (PV) 
% Nb = nombre de pixel d'un cote de la scene carree
% SORTIE :
%  pupille_complexe = fonction complexe de la pupille
%      amplitude.*exp(2ipi*phase_lambda)

%--------------------------------------------------------------------------

%Les arguments doivent etre au complet :
narginchk(3,3)

%%% Construction de la grille de la pupille
S=2+1/Nb;
pech_pup = 2*S/Nb; % pas d'echantillonnage de la pupille
x = -S:pech_pup:S-pech_pup ; %(suppose Nb pair)
X = repmat(x,[length(x) 1]); %plus rapide que meshgrid
Y = repmat(x',[1 length(x)]);
RHO2 = X.^2+Y.^2; %sans unite
RHO = sqrt(RHO2);
pup = RHO2<1; %la ou l'ouverture se trouve (pupille circulaire)
SGN = (X>=0)-(X<0);

%%% Calcul de l'amplitude (pupille circulaire)
amplitude = zeros(size(X)) ;
amplitude(pup) = 1;

%%% Phase donnee par le masque :

phase_masque = zeros(size(RHO)) ;
delta_phase = 1/2; %on calcule la phase en lambda (donc une marche de phase de pi vaut 1/2 lambda)

if (length(param_masque)>=1)
    
    %Remplissage de la phase :
    % - alternance entre 0 et delta_phase
    % - la phase au centre (RHO < param_masque(1)) vaut 0 
    
    nb_rayons = length(param_masque) ;
    for k=2:2:nb_rayons
        phase_masque(RHO>=param_masque(k-1) & RHO<param_masque(k)) = delta_phase;
    end
    if (mod(nb_rayons,2) == 1)
        phase_masque(RHO>=param_masque(nb_rayons) & RHO<=1 ) = delta_phase;
    end
end
        
%%% Phase donnee par le defocus 
phase_psi = RHO2 * psi;

%%% Phase = masque + defocus
phases_lambda_brute = phase_psi + phase_masque ; 

%Amplitude complexe dans la pupille :
pupille_complexe = amplitude .* exp(2i*pi*phases_lambda_brute);

end

