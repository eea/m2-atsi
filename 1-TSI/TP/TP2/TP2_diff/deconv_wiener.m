%%% Deconvolution par le filtre de Wiener. 
%%% (On suppose que le bruit est blanc)
%%% Attention ! Les DSP signal et bruit sont estim�es � partir de l'image 
%%% nette et du filtre. 
%%%
%%% usage : y=deconv_wiener(im,OTF,im_nette,SNR) 
%%% 
%%% Entree:
%%% im : image d'entree
%%% OTF : OTF (r�ponse en fr�quence) du filtre
%%% im_nette : image non d�convolu�e
%%% SNR : niveau de bruit, suppos� blanc, en dB par rapport � l'image nette
%%% convolu�e par OTF
%%% Sortie:
%%% y : image d�convolu�e
%%% 

function y=deconv_wiener(im,OTF,im_nette,SNR)

%%% Dimensions de l'image (on suppose qu'elle est carr�e)
[M N]=size(im_nette);

%%% DSP image nette, moyenne nulle
Sxx = abs(fft2(im_nette)).^2; 
Sxx=Sxx/sum(sum(Sxx));
Sxx(1,1)=0;

%%% DSP bruit
E_f = sum(sum(abs(OTF).^2.*Sxx));
s2 = E_f * 10^(-SNR/10);
Sbb = s2 / N^2;

%%% TF de l'image 
tf_im = fft2(im);

%%% Calcul de l'image d�convolu�e
tf_y = tf_im .*  (conj(OTF) .* Sxx) ./ (abs(OTF).^2 .* Sxx + Sbb);
tf_y(1,1) = tf_im(1,1); % traitement particulier pour la moyenne

y=real(ifft2(tf_y));




