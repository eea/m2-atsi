%%% Perturbation d'un signal par un gruit gaussien  
%%% additif de moyenne nulle avec un rapport signal
%%% sur bruit donn�
%%%
%%% usage : y=bruite_snr(im,snr) 
%%% 
%%% Entree:
%%% im : donn�es d'entr�e (tableau � une ou deux dimensions)
%%% snr : valeur du rapport signal sur bruit (en dB)
%%% Sortie:
%%% y : image bruit�e avec un rapport signal sur bruit
%%%     �gal � snr.
%%% 

function y=bruite_snr(im,snr)

% Variance de l'image 
VAR_im = var(im(:));

% Calcul de la variance du bruit gaussien

VAR_bruit = VAR_im * 10^(-snr/10);

% Bruitage de l'entr�e
y = im + sqrt(VAR_bruit)*randn(size(im));



