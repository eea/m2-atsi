%%% Convolution d'une image avec un filtre sp�cifi� par son OTF.
%%% La convolution est faite en passant dans l'espace de Fourier
%%%
%%% usage : y=convtf(OTF,im) 
%%% 
%%% Entree:
%%% OTF : OTF (r�ponse en fr�quence) du filtre 
%%% im : image
%%% Sortie:
%%% y : r�sultat de la convolution
%%% 

function y=convtf(OTF,im)

y = real(ifft2(OTF.*fft2(im)));