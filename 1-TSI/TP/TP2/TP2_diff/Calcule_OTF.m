function [OTF,PSF,pupille_complexe] = Calcule_OTF(radii,psi,Nb)
%--------------------------------------------------------------------------
% USAGE : 
%  [OTF,PSF,pupille_complexe] = Calcule_OTF(radii,psi,Nb)

% DESCRIPTION :
%  calcule l'OTF, la PSF et la pupille complexe d'un systeme en limite de
%  diffraction avec un masque de phase, sur une plage de defocalisation

% ENTREE :
%  param_masque = tableau contenant les rayons normalises 
%  psi = valeur du defocus \psi en \lambda (PV) sur lequel
%  Nb = nombre de pixel d'un cote de la scene carree


% SORTIE :
%  OTFs = OTF NbxNb
%  PSFs = PSF NbxNb 
%  pupille_complexe = pupille complexe NbxNb 
%--------------------------------------------------------------------------

%Les arguments doivent etre au complet :
narginchk(3,3)

%Calcul de la pupille :
pupille_complexe = Fonction_Pupille( radii,psi,Nb);

%Calcul de la PSF :
PSF = Calcule_PSF( pupille_complexe);

% Calcul de l'OTF :
OTF = fft2(PSF);
end