\documentclass[11pt, a4paper]{article}

\usepackage[nottoc]{tocbibind}
\usepackage{amsthm}

\usepackage{amssymb}

\newtheorem{theorem}{Theorem}
\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\usepackage{tabto}

\usepackage[noend]{algpseudocode}

\usepackage[ruled, vlined]{algorithm2e}

\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}
\usepackage{color}
\definecolor{ENSblue}{RGB}{0,119,139}
\usepackage{soul}
\usepackage{ulem}
\usepackage{url}
\usepackage{setspace}
\usepackage[backref=true,colorlinks=true,linkcolor=ENSblue,urlcolor=ENSblue]{hyperref}
\usepackage{fancybox}
\usepackage{verbatim}
\usepackage{graphicx} 
\usepackage{caption}
\usepackage{subcaption}
\usepackage{tabularx} 
\usepackage{setspace}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{eurosym}
\usepackage{eso-pic,xcolor,graphicx}
\usepackage{listings}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{float}
\usepackage{fancyhdr}
    \fancyhf{} % efface le style par defaut de fancy
    \pagestyle{fancy}
    \rfoot{\thepage}
    \lfoot{ENS - Paris Saclay}
    \lhead{M2 ATSI - Signal Processing and Imaging Systems}
    \rhead{\phantom{x}}
\usepackage{dsfont}
    
\definecolor{darkWhite}{rgb}{0.94,0.94,0.94}
\definecolor{greenleo}{rgb}{0.34, 0.68, 0.21}
\newcommand{\norm}[1]{\left\lvert#1\right\rvert}
\newcommand{\sinc}[1]{\text{sinc}(#1)}
\renewcommand{\thesection}{\arabic{section}}
% \renewcommand{\thesubsection}{\arabic{subsection}}

\lstset{
  aboveskip=3mm,
  belowskip=-2mm,
  backgroundcolor=\color{darkWhite},
  basicstyle=\footnotesize,
  breakatwhitespace=false,
  breaklines=true,
  captionpos=t,
  commentstyle=\color{greenleo},
  deletekeywords={...},
  escapeinside={\%*}{*)},
  extendedchars=true,
  framexleftmargin=16pt,
  framextopmargin=3pt,
  framexbottommargin=6pt,
  frame=single,
  keepspaces=true,
  keywordstyle=\color{blue},
  language=Matlab,
  literate=
  {²}{{\textsuperscript{2}}}1
  {⁴}{{\textsuperscript{4}}}1
  {⁶}{{\textsuperscript{6}}}1
  {⁸}{{\textsuperscript{8}}}1
  {€}{{\euro{}}}1
  {é}{{\'e}}1
  {è}{{\`{e}}}1
  {ê}{{\^{e}}}1
  {ë}{{\¨{e}}}1
  {É}{{\'{E}}}1
  {Ê}{{\^{E}}}1
  {û}{{\^{u}}}1
  {ù}{{\`{u}}}1
  {â}{{\^{a}}}1
  {à}{{\`{a}}}1
  {á}{{\'{a}}}1
  {ã}{{\~{a}}}1
  {Á}{{\'{A}}}1
  {Â}{{\^{A}}}1
  {Ã}{{\~{A}}}1
  {ç}{{\c{c}}}1
  {Ç}{{\c{C}}}1
  {õ}{{\~{o}}}1
  {ó}{{\'{o}}}1
  {ô}{{\^{o}}}1
  {Õ}{{\~{O}}}1
  {Ó}{{\'{O}}}1
  {Ô}{{\^{O}}}1
  {î}{{\^{i}}}1
  {Î}{{\^{I}}}1
  {í}{{\'{i}}}1
  {Í}{{\~{Í}}}1,
  morekeywords={*,...},
  numbers=left,
  numbersep=10pt,
  numberstyle=\tiny\color{black},
  rulecolor=\color{black},
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  stepnumber=1,
  stringstyle=\color{gray},
  tabsize=4,
  title=\lstname,
}

\begin{document}
\begin{titlepage}
		\hfill
		\includegraphics[height=0.1\textheight]{g3874.png}
		\vspace{-2cm}
		\vfill
		\begin{center}
		\begin{doublespacing}
			\Large{\textbf{Signal processing and imaging systems}}\\
			\Large{\textbf{Labwork 2}}\\
			{Optimization of annular binary phase masks for depth-of-field extension}
			\vspace{1cm}
			\Large{\textit{\textbf{}} \\}
			\vspace{1cm}
			\normalsize{\textsc{ENS Paris-Saclay -- Centrale Supelec \\ Maxence PROST - Pierre Antoine COMBY}  \\}
			\vspace{1cm}
			\normalsize{--\textsc{2020/2021}  --\\}
			\vspace{1cm}
		\end{doublespacing}
		\end{center}
		\vfill

	\end{titlepage}
	
%\tableofcontents

The objective is to optimize annular binary phase masks for depth-of-field extension. Such masks consist of rings introducing a dephasing of alternatively 0 or $\pi$, or, equivalently, an optical path length 0 or $\lambda/2$, where $\lambda$ is the wavelength of light (light is assumed monochromatic).The parameters are the radii of the rings (cf. subject).

\paragraph{Q1 \& Q2}
We can also observe the OTF and PSF in the presence of the mask, for various radii.

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.6\textwidth]{1./OTF_PSF.png}
    \caption{OTF or various $r$ and $\psi$}
    \label{fig:OTFrpsi}
\end{figure}

We can see on Figure \ref{fig:OTFrpsi} that we have parity on the phase, and that the bigger the phase, the narrower are its fluctuations. Moreover, as presented, having multiple radii increases the number of ring, and in the absence of mask ($r=0$), we have the widest centrale cone, hence the widest PSF.

\paragraph{Q3}
From now on, we will consider a phase mask with two rings, and $r_1=0.5$. We will work on the image \texttt{butterfly.tiff}.

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.6\textwidth]{1./q3.png}
    \caption{Observation of the image with and without mask with various defocus}
    \label{fig3}
\end{figure}
One can observe that with the mask, for low values of defocus, the observation seems to be of a lower quality than without the mask. However when $\psi=1$, the quality is visualy better with the mask. It emphasizes its the utility: for a given depth of field, the image without mask is better, when there are several depth of field, the observation with the mask gives a better representation of all the depth of field, unlike  the no-mask case where only plan of the depth of field is observable and the others are blurry.

Two issues can be emphasized here. First, the value of the radii are not optimized. Secondly, the deconvolution is optimized for only one defocus value.

\paragraph{Q4}

We will now study deconvolution with the Wiener filter of an image convoluted and perturbed with noise. We generate the image observed with the same mask, for $\psi=0.5$, and we observe the images with and without noise.

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{1./q4.1.png}
    \caption{Observation of the image with mask}
    \label{fig3}
\end{figure}
Since the SNR is high, not much difference can be observed. If we take a lower SNR, the influence of the additive noise can be observed.
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{1./q4.2.png}
    \caption{Observation of the image with mask}
    \label{fig3}
\end{figure}

Using a SNR of 40dB, we will now perform the deconvolution of the image with the Wiener filter, and compute the reconstruction quality.
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.4\textwidth]{1./q4.3.png}
    \caption{Observation of the image with mask}
    \label{fig3}
  \end{figure}

Visually, the reconstruction is satisfying. The RQ is quite high, meaning the reconstruction is correct (compared to some RQ values obtained in other image processing techniques).
\paragraph{Q5}
The objective is to improve the depth of field of an observation system. We will thus perform deconvolution with the averaged Wiener filter computed from OTF obtained at $\psi\in\{0,0.5,1\}$. The new function is based on the previous function. The modification are the following : the SBB is computed from the OTF used for the convolution. The Wiener filter is created using the average of the three OTF obtained from the three values of $\psi$.

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{1./q5.1.png}
    \caption{Averaged Wiener with r=0.5}
    \label{fig3}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{1./q5.2.png}
    \caption{Averaged Wiener without mask r=0}
    \label{fig3}
\end{figure}

Without mask, the average RQ is 13.1dB, against 13.7dB with mask. What we can observe is that without mask, the resolution is satisfying for one defocus value only, which means one can only observe one field on the image. With the mask, the RQ is ``distributed'' the different defocus values which means one can observe the different planes of the image.

\paragraph{Q6}
In reality, the value of $\psi$ used in the observation is unknown. We want to find the optimal value for the radius $r_1$ of the mask. To do so, we will apply the deconvolution for each value of $\psi$, compute RQ and create a function equal to the minimum of the three results for each value of $r_1$. Since the value of $\psi$ is unknown, we want to maximize the minimum of the functions RQ=f($r_1$), to provides the best reconstruction  in the worst case:

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.6\textwidth]{1./q6.1.png}
    \caption{RQ against $r_1$ for all $\psi$}
    \label{fig3}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.5\textwidth]{1./q6.2.png}
    \caption{minimum of the RQ}
    \label{fig3}
\end{figure}
The optimal radius is found with a maxmin approach. We then use the optimzed mask.
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{1./q6.3.png}
    \caption{optimized mask r=0.823}
    \label{fig3}
\end{figure}
As expected, the RQ are maximum for all values of $\psi$, which means no matter the defocus, we have the cleanest image possible, with all the image plans visible.

\paragraph{Q7}
Of course, the higher the amount of radii, the better the result. We will now consider a 3-ring mask defined by $r_1$ and $r_2$. The method is the same.
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{1./q7.1.png}
    \caption{function minimum of the RQ ($r_1$ and $r_2$ against RQ)}
    \label{fig3}
\end{figure}
And as expected, the RQ is even better for each defocus value.

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.6\textwidth]{1./q7.2.png}
    \caption{function minimum of the RQ ($r_1$ and $r_2$ against RQ): $r_{opt}=[0.7895~ 0.9474]$}
    \label{fig3}
\end{figure}

We could go on with a higher dimension mask. However, the calculation of the optimal values of the radii become trickier above 3D.

\newpage
\appendix
\section{Preliminary}
Let's consider the function 
\begin{equation}
    \Pi_T(u)=\left\{\begin{matrix}1\mbox{ si }u\in[0,T]\\0 \mbox{ sinon}\end{matrix}\right.
\end{equation}

One can easily show that $TF(\Pi_T(u))=e^{-i\pi fT}Tsinc(\pi fT)$. We can plot an example for $T=2$.
Some proprties of this FT:
\begin{itemize}
  \item The maximum is reached for f=0, and is worth T.
\item $\{f \setminus TF(\Pi_T(u))=0\}=\{f \setminus fT=k, k\in \mathbb{N}\}$
\end{itemize}
We can also study the two dimensional situation. Let's define $f(u,v)=\Pi_{T_1}(u)\Pi_{T_2}(v)$. By separation of integrals, one can easily conclude that $TF[f(u,v)](f,g)=e^{-i\pi (fT_1 + gT_2)}sinc(\pi fT_1)sinc(\pi gT_2)$.

\section{Fourier Transform and image convolution}
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.5\textwidth]{Rect1_2.png}
    \caption{Rect1 and Rect2}
    \label{fig1}
\end{figure}

Let's compute the FFT of images RECT1 and RECT2. We will use a logarithm scale for the module which will obviously improve contrasts since the log function "decreases" the high values.
\begin{figure}[H]
    \centering
    \includegraphics[width = 1\textwidth]{FFT_rect.png}
    \caption{FFT of Rect1 and Rect2}
    \label{fig3}
\end{figure}

This result is interesting. It is worth reminding that in 1D, the FFT of $\Pi_T$ is a cardinal sine. In 2D, a rectangle can be seen as the product of two of these functions, in each direction. Thus, the FFT of these rectangles will give a superposition of cardinal sines in each direction, whose amplitude is here represented with the colors. In regard to what was said in the preliminary, the width of the first lobe is equal to $\frac{2}{T}$, $T$ being the non zero time of the rectangle. Since in Rect1, the size of a rectangle on the horizontal axis is smaller than in Rect2, the width of the first lobe will be higher. However, on the vertical axis, the size of the rectangle is the same, and so is the distance between two zeros values of the FFT.

\vspace{0.5cm}
We will now build a function to perform the convolution between two images. We will perform this convolution between Rect and Palaiseau. To do so, we perform the FFT of the product of both images, and then perform the IFFT.

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.6\textwidth]{Palaiseau.png}
    \caption{Palaiseau}
    \label{fig3}
\end{figure}

And the convolution result :

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.6\textwidth]{Convolution.png}
    \caption{Palaiseau}
    \label{fig3}
\end{figure}

Let's notice that a convolution in the time domain is a product in Fourier domain. We have already seen the FFT of Rect. Let's observe the FFT of Palaiseau.

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.6\textwidth]{FFT_palaiseau.png}
    \caption{Palaiseau}
    \label{fig3}
\end{figure}

\section{Deconvolution}

Let's assume that the image measured by the sensor is $y=y_0+b=h\ast x+b$ where $y_0$ is the image without noise, and $b$ the noise. We have
\begin{equation}
    RSB_{in}=10\times \mbox{log}_{10}\left[\frac{\mbox{Var}[y_0]}{\mbox{Var}[b]}\right]
\end{equation}
We can easily create a function that convolutes and add noise to an image, for a given $RSB_{in}$. We can see the result for different values of $RSB_{in}$.

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{RSBin.png}
    \caption{Convolution and Noise}
    \label{fig3}
\end{figure}

One can observe that when the RSBin is higher, the quality of the image is better in a sense that there is less noise. Indeed, the higher RSBin, the higher the quotient between the variance of the image and the variance of the noise. We took an additive noise, so it doesn't has any effect on the convolution.

We are now using a deconvolution function where the regularization parameter $\lambda$ is an entry parameter.

The reconstructed image is expressed as $\hat{y}_0=FFT^{-1}\left(\Tilde{w}(\mu,\nu)FFT(y)\right)$ where $\Tilde{w}(\mu,\nu)=\frac{\Tilde{h}^*(\mu,\nu)}{|\Tilde{h}(\mu,\nu)|^2+\lambda}$. We can make a few remarks. First, if $\lambda=0$, $\Tilde{w}$ is an inverse filter. If $\lambda >> |\Tilde{h}(\mu,\nu)|$, high frequencies are attenuated. This filter minimizes the least square error.

The process is thus to take the FFT of $y$, multiply it by the filter and take the IFFT of the result. It is interesting to study the influence of $\lambda$.

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{deconv_lambda.png}
    \caption{Influence of $\lambda$ on the deconvolution}
    \label{fig3}
\end{figure}

We can observe that the results are different for different values of the regularization parameter. When $\lambda$ is small, we can observe some artefacts due to a bad regularization, we are close to a inverse filter (cf. Figure \ref{artefacts}).

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.6\textwidth]{artefacts.png}
    \caption{}
    \label{artefacts}
\end{figure}

On the other hand when $\lambda$ is too high, the high frequencies are attenuated and the result is not good either. We thus have to determine the optimal value of $\lambda$.

\section{Application : restoration of a motion blur}
Let $x(u,v)$ be the observed image when the scene is motionless. Let's assume that during T (s), the object moves at a speed V in the horizontal direction.

We consider the degradation model $y(u,v)=\int_0^Tx(u-Vt,v)dt$. To use the Wiener filter, it requires $\Tilde{w}(\mu,\nu)$.
\begin{align*}
    \Tilde{y}(\mu,\nu)&=\int_\mathbb{R}\int_\mathbb{R}y(u,v)e^{-2i\pi (\mu u+\nu v)}dudv\\
    &=\int_\mathbb{R}\int_\mathbb{R}\left[\int_0^Tx(u-Vt,v)dt\right]e^{-2i\pi (\mu u+\nu v)}dudv\\
    &=\int_0^T\left[\int_\mathbb{R}\int_\mathbb{R}x(u-Vt,v)dte^{-2i\pi (\mu u+\nu v)}dudv\right]dt\\
    &=\int_0^T\Tilde{x}(\mu,\nu)e^{-2i\pi \mu Vt}dt\\
    &=\Tilde{x}(\mu,\nu)\int_0^Te^{-2i\pi \mu Vt}dt\\
    &=\Tilde{x}(\mu,\nu)\Tilde{h}(\mu,\nu)
\end{align*}
So we obtain 
\begin{align*}
    \Tilde{h}(\mu,\nu)&=\int_0^Te^{-2i\pi \mu Vt}dt\\
    &=\left[ \frac{e^{-2i\pi \mu Vt}}{-2i\pi \mu VT} \right]_0^T\\
    &=e^{-i\pi\mu VT}T\mbox{sinc}(\pi \mu VT)
\end{align*}
According to the preliminaries, this is the FFT of $h(u,v)=\Pi_T(u)\delta(v)$.

We now have to determine the blur, i.e. the quantity $VT$ of pixels. We will use the FFT of the image to restore to determine the product $VT$.
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{texte.png}
    \caption{Image to restore}
    \label{fig3}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{fft_texte.png}
    \caption{FFT of the image}
    \label{fig3}
\end{figure}
We can observe an horizontal shift, and the FFT shows an horizontal periodicity corresponding to the inverse of the size of the kernel. Indeed, a temporal convolution equals a frequential multiplication, thus we observe the FFT of our image times the FFT of the convolution Kernel (i.e. a 'door' function in time domain, a sinc in frequential domain).
We can see an horizontal periodicity of \texttt{10 px}, with a fft which is \texttt{640 px} wide, we conclude that $VT=64$ pixels.
\vspace{0.5cm}
\\Thus, we now have the convolution kernel, which has the following FFT :

\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{convolution_kernel.png}
    \caption{Convolution Kernel}
    \label{fig3}
\end{figure}

And applying the result of the previous section, we can restore the text, and we obtain
\begin{figure}[H]
    \centering
    \includegraphics[width = 0.8\textwidth]{Restored_text.png}
    \caption{Restored text}
    \label{fig3}
\end{figure}
Let's remark that the value of $\lambda$ taken was around 1. This value is relative and depends on the PSF. For instance is the PSF is multiplied by 10, the optimal value of $\lambda$ is multiplied by 100. 
\end{document}
