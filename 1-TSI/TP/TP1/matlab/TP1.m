clear all;
close all;
% TP 
a           = 1;                      % amplitude de la PSF
w           = 2;                      % FWHM de la PSF (en µm)
sigs        = w/(2*sqrt(2*log(2)));   % ecart-type de la PSF (en µm)
Dx          = 2  ;                  % Pas de discretisation des mesures (en µm)
th          = linspace(45,60,200);    % postion de l'emetteur (en µm)
test_theta  = [46 46.2 46.5 47 47.2 48];      % valeurs de theta
sigb        = 0.01;                   % ecart-type du bruit
N           = 1000;                    % nombre de pixels
I           = linspace(0,N-1,N);      % indices des pixels
X           = linspace(I(1)*Dx,I(end)*Dx,N*10); 
                                      % position pour tracer l'exponentielle non iscretisee
nb_realis   = 100;                   % nombre de realisations
% ----------------------------------
gauss = @(x,sig) 1./(sqrt(2*pi)*sig)*exp(-0.5*x^2/sig^2);
r = @(x) gauss(x,sigs);
%% CRLB basics

figure(1);
CRLB = NaN(length(th),1);
r = @(x) gauss(x,sigs);
for k=1:length(th)
    dri=  arrayfun(r,(I.*Dx)-th(k));
    CRLB(k) = (sigb/a)^2./sum((dri(1:end-1)-dri(2:end)).^2);
end
plot(th,CRLB);


%% CRLB en fonction de W

figure;
clf;
for wrange = 1:4
    sigs= wrange/(2*sqrt(2*log(2)));   % ecart-type de la PSF (en µm)
    CRLB = NaN(length(th),1);
    r = @(x) gauss(x,sigs);
    for k=1:length(th)
        dri=  arrayfun(r,(I.*Dx)-th(k));
        CRLB(k) = (sigb/a)^2./sum((dri(1:end-1)-dri(2:end)).^2);
    end
    plot(th,CRLB);
    hold on;
end
xlabel('\theta');ylabel('CRLB');
legend('w=1','w=2','w=3','w=4');
%%  max de la CRLB en fonxtion de W
figure;
clf;
wrange = linspace(1,10,30);
CRLBm = NaN(30,1);
for i=1:30
    sigs= wrange(i)/(2*sqrt(2*log(2)));   % ecart-type de la PSF (en µm)
    CRLB = NaN(length(th),1);
    r = @(x) gauss(x,sigs);
    for k=1:length(th)
        dri=  arrayfun(r,(I.*Dx)-th(k));
        CRLB(k) = (sigb/a)^2./sum((dri(1:end-1)-dri(2:end)).^2);
    end
    CRLBm(i) =  max(CRLB);
end
[val,idx] =min(CRLBm);
plot(wrange,CRLBm);
xlabel('FWHM');
ylabel('max(CRLB)');
title(strcat(['min(max(CRLB))= ' num2str(val,'%.2e') '\\to  w_{opt}=' num2str(wrange(idx),2)]));

%% Estimateur ML 

th = linspace(25,75,100);
Dx = 2;
thetaTrue = 60;

bi =randn(1,N)*sigb;
ri = PSFdiscrete(sigs,Dx,thetaTrue,I);
si = a* ri + bi;
figure(1);
clf;
Lh = @(x) sum((si-a.*PSFdiscrete(sigs,Dx,x,I)).^2);
thetaGuess = fminsearch(Lh, 55);
plot(th,1.2-arrayfun(Lh,th));
title(strcat(['\theta =' num2str(thetaGuess)]));



%% biaisé ? consistent ?
th = linspace(25,75,100);
Dx = 2;
thetaTrue = 60;
Nreals = 1e6;
thetaGuess = NaN(Nreals,1);
aGuess = NaN(Nreals,1);
for k =1:Nreals
bi =randn(1,N)*sigb;
ri = PSFdiscrete(sigs,Dx,thetaTrue,I);
si = a* ri + bi;
Lh = @(x) sum((si-a.*PSFdiscrete(sigs,Dx,x,I)).^2);
thetaGuess(k) = fminsearch(Lh, 59);
aGuess(k) = sum(ri.*si)./sum(ri.^2); 
end
for n=1:6
disp([mean((thetaGuess(1:10^n)-60)), var(thetaGuess(1:10^n))]);
disp([mean((aGuess(1:10^n)-1)), var(aGuess(1:10^n))]);
end 

%% influence de a
CRLB = NaN(length(th),1);
CRLB = NaN(length(th),1);

r = @(x) gauss(x,sigs);
for k=1:length(th)
    ri = PSFdiscrete(sigs,Dx,th(k),I);
    dri=  arrayfun(r,(I.*Dx)-th(k));
    s2dri = sum((dri(1:end-1)-dri(2:end)).^2);
    CRLBtheta(k) = (sigb/a)^2.*sum(ri(1:end-1).^2)./(s2dri.*sum(ri(1:end-1).^2) - sum((dri(1:end-1)-dri(2:end)).*ri(1:end-1)).^2);;
    CRLBa(k) = (sigb)^2.*s2dri./(s2dri.*sum(ri(1:end-1).^2) - sum((dri(1:end-1)-dri(2:end)).*ri(1:end-1)).^2);
end
figure;
plot(th,CRLBtheta);
xlabel('\theta');
ylabel('CRLB_\theta');
figure;
plot(th,CRLBa);
xlabel('a');
ylabel('CRLB_a');
%% Estimateur de A

