%%%%%
% Script pour TP03 - 2013-2014
%%%%%

clear variables
close all

tic

% ----------------------------------
%%% Param�tres
a           = 1;                      % amplitude de la PSF
w           = 2;                      % FWHM de la PSF (en �m)
sigs        = w/(2*sqrt(2*log(2)));   % ecart-type de la PSF (en �m)
Dx          = 2; %2                     % Pas de discretisation des mesures (en �m)
th          = linspace(40,60,200);    % postion de l'emetteur (en �m)
test_theta  = [46 46.2 46.5 47 47.2 48];      % valeurs de theta
%test_theta  = linspace(46,52,6);      % valeurs de theta
sigb        = 0.01;                   % ecart-type du bruit
N           = 100;                    % nombre de pixels
I           = linspace(0,N-1,N);      % indices des pixels
X           = linspace(I(1)*Dx,I(end)*Dx,N*10); 
                                      % position pour tracer l'exponentielle non iscretisee
nb_realis   = 100;                   % nombre de realisations
% ----------------------------------

%%

%%%%%
% 1. Location estimation
%%%%%


% ----------------------------------
%%% Profil des PSF
PSFs = NaN(length(X),length(test_theta));
PSFi = NaN(length(I),length(test_theta));
for k=1:length(test_theta)
   PSFs(:,k) = Dx/(sqrt(2*pi)*sigs)*exp(-0.5*(X-test_theta(k)).^2/sigs^2);
    % calcul de la PSF 'echantillonee'
   PSFi(:,k)=PSFdiscrete(sigs,Dx,test_theta(k),I);
    % calcul de la PSF 'integree'
   figure(1)
   subplot(2,3,k)
   bar((I+0.5)*Dx,PSFi(:,k))
    % on decale d'un demi cran car la valeur du "haut" du rectangle ne doit
    % pas etre reporte au milieu de l'intervalle [i, i+1]
   hold on
   plot(X,PSFs(:,k),'r')
   title(num2str(test_theta(k)));
   xlabel('Position (�m)');
   ylabel('Signal (a.u.)');
   %axis([40 60  0 max(PSFs(:,k))])
   axis([44 52  0 max(PSFs(:,k))])
   grid on
end
% ----------------------------------

