clear all;
close all;
%% Transformée de fourier  identiques
f1 = 20;
f2 = 400;
L = 1000;

t = linspace(0,1,L);
s = [sin(f1*t(1:end/2)) sin(f2*t(end/2+1:end))];
s2 = [0.5*sin(f1*t)+0.5*sin(f2*t)];

fig = figure(1);
subplot(221);
plot(t,s);
subplot(222);
plot(t,s2);

Y = fft(s);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
subplot(223);
plot(P1);
axis padded;


Y = fft(s2);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);

subplot(224);
plot(P1);
axis padded;

matlab2plot('plot/note_fft.tikz',fig,'relativeDataPath','data','DataPath','../data');
%% Cosinus locaux

